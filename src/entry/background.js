//Handle extension events
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if(request.command == "translate-request")
    {
    	//redirect it to content script  
    	chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
	        chrome.tabs.sendMessage(tabs[0].id, {"command": "translate-request"}, function(response) {
	            console.log(response);
	        });
	    });     
    } 
    if(request.command == "update-vote")
    {
    	//redirect it to content script  
    	chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
	        chrome.tabs.sendMessage(tabs[0].id, {"command": "update-vote", "num":request.num}, function(response) {
	            console.log(response);
	        });
	    });     
    } 
    if(request.command == "server-request")
    {
    	fetch(request.serverUrl, {
		    method: 'POST',
		    body: JSON.stringify(request.body),
		    headers: request.headers,
	  	})    
    } 
});