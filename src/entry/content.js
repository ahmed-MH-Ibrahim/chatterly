import $ from "jquery";
import swal from 'sweetalert2';
import  '../assets/css/content.css';
import { createApp } from 'vue';
import TranslatePopup from '../view/translate.vue';
import TranslateResultPopup from '../view/translate-result.vue';

//global vars
var url = window.location.href;
var serverUrl = "https://chatterlyai.net/server.php";
var currentSelection,storedSelection,currentRange,storedRange;
var rangySelection,selectedElement;
var selectableElements = [], areElementsLoaded = false;
var currentMousePos = { x: -1, y: -1 };
var popupYMargin = 25;
var textRequested = '';
var textHighlighted = false;
var hasSelectionChanged = false, cachedNote = false;
var maxSringLimit = 1000000;
var isChatterlyClicked = false;
var translateSettingsApp, translateResultApp;
var currentTextElement; //stores current active text element on a webpage

//handle extension events
  chrome.runtime.onMessage.addListener(function(request, sender, sendResponse)
  {
    if(request.command == "vote-settings-change")
    {
    	translateResultApp.isRatingEnabled = request.value;           
    }
    else if(request.command == "autotranslate-settings-change")
    {
      	translateSettingsApp.isTranslateEnabled = request.value;       
    }
    else if(request.command == "translate-request")
    {
    	translateAndUpdateField(); 
    	     
    }
    else if(request.command == "update-vote")
    {
    	updateVote(request.num); 
    	     
    }
  });

//detect storage changes to update settings live
chrome.storage.onChanged.addListener(function(changes, namespace) {

     //handle translateion settings view changes to reflect on translation results view
     if(changes.ratingEnabled!= undefined && translateResultApp.isRatingEnabled != changes.ratingEnabled.newValue )
     {
     		translateSettingsApp.isRatingEnabled = changes.ratingEnabled.newValue;
     		translateResultApp.isRatingEnabled = changes.ratingEnabled.newValue;
     }
});

$(document).ready(function(){

	//appending stylesheets
	$('body').append('<link rel="stylesheet" type="text/css" href="'+chrome.runtime.getURL('css/content.css')+'">');

	$("body").append("<div id='chatterly-translate-ui-popup-container' style='display:none;' class='chatterly-translate-ui-popup-container'></div>");
	//append translate popup vue to site dom
	translateSettingsApp = createApp(TranslatePopup).mount('#chatterly-translate-ui-popup-container');

	$("body").append("<div id='chatterly-translate-result-ui-popup-container' style='display:none;' class='chatterly-translate-result-ui-popup-container'></div>");
	//append translate result popup vue to site dom
	translateResultApp = createApp(TranslateResultPopup).mount('#chatterly-translate-result-ui-popup-container');

	//detect key presses
	$( document ).keydown(async function(event) {
	  	if (translateSettingsApp.isTranslateEnabled && event.key === 'q')
			{
	      if(event.ctrlKey)
	      {
	        var focused = document.activeElement;
	        var text = $(focused).val();

	        if(text == undefined || text == "")
	        	text = $(focused).text();

	        translateRequest(text,2,$(focused));
	        
	      }
			}
	});	

	//handling slack
	if(url.includes("slack"))
    {
    	slackHandler();
    }

    textareaHandler();

    //handle text select
    document.addEventListener('select', handleSelection);

    //handle mouse coordinates
    $(document).mousemove(function(event) {
		if(!textHighlighted)
		{
	        currentMousePos.x = event.pageX;
	        currentMousePos.y = event.pageY;
	    }
    });

    //handle mouse clicks to remove popup to improve user experience
    $(document).on('click', function(e){
    //$(document).on("mouseup",function(e){

		if(isChatterlyPopupClicked(e))
			return;				

		closeChatterlyPopup();

		handleSelection();      
	});

	//reset on mousedown click
	$(document).on("mousedown",function(e){
		if(isChatterlyPopupClicked(e))
			return;				

		closeChatterlyPopup();
	});

	
	$("#chatterly-translate-result-ui-popup-container, #chatterly-translate-ui-popup-container").click(function(event){
		isChatterlyClicked = true;
		//event.stopPropagation();
	})

	//handle keypress to hide ui to improve user experience
	$(document).keydown(function() {
	  	closeChatterlyPopup();
	});

	//handle scrolling to hide ui to improve user experience
	$('body').on('mousewheel', function (e) {
		if(isChatterlyPopupClicked(e))
			return;
	    closeChatterlyPopup(true);
  	});

	//handle dynamic content loading/change on dom to include newly loaded textareas/fields
	MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

	var observer = new MutationObserver(function(mutations, observer) {
	    // fired when a mutation occurs
	   
	    if(url.includes("slack"))
	    {
	    	slackHandler();
	    }

	    textareaHandler();
	});

	// define what element should be observed by the observer
	// and what types of mutations trigger the callback
	observer.observe(document, {
	  subtree: true,
	  attributes: true
	});




});

//function to handle popup click detection
function isChatterlyPopupClicked(e)
{
	if($(e.target).hasClass("chatterly-translate-btn") || $(e.target).hasClass("chatterly-translate-ui-popup-container") || $(e.target).parents(".chatterly-translate-ui-popup-container").length || $(e.target).hasClass("chatterly-translate-result-ui-popup-container") || $(e.target).parents(".chatterly-translate-result-ui-popup-container").length)
		return true;

	return false;
}
//handles UI for slack and its text/div containers
function slackHandler()
{
	//load stored data
    chrome.storage.local.get(['catchyEnabled'],function(obj){

      	if(obj == undefined || obj.catchyEnabled == undefined || obj.catchyEnabled == true)
      	{
	        $(".c-texty_input_unstyled__container").each(function(){

						if($(this).hasClass("chatterly-ui-handler"))
							return;

						var textElement = $(this);

						textElement.append(
							$('<img src="'+chrome.runtime.getURL('img/icon.png')+'" height="25px" width="25px" class="chatterly-translate-btn">').on('click', function(e){

						      	// translate btn click handler
						      	handleChatterlySettingsClick($(this));
						    })
						)

						textElement.addClass("chatterly-ui-handler");

					})
      	}  
      	else{
      		//remove image icons and handlers if extension is disabled
      		$(".chatterly-translate-btn").remove();
      		$(".chatterly-ui-handler").removeClass("chatterly-ui-handler");
      	}    

    });
	
}

function textareaHandler(){

	//load ui only if enabled
	chrome.storage.local.get(['catchyEnabled'],function(obj){

      	if(obj == undefined || obj.catchyEnabled == undefined || obj.catchyEnabled == true)
      	{
	        $("textarea,input[type='text'],input[type='search']").each(function(){

						if($(this).hasClass("chatterly-ui-handler") || $(this).attr("disabled") || $(this).attr("readonly"))
							return;

						var textElement = $(this);

						//handling upwork textarea
						if(textElement.is("textarea") && textElement.parents(".msg-composer-maxheight").length)
						{
							textElement.parents(".msg-composer-maxheight").prev().prepend(
								$('<img src="'+chrome.runtime.getURL('img/icon.png')+'" height="25px" width="25px" class="chatterly-translate-btn chatterly_position_static">').on('click', function(e){

							      	handleChatterlySettingsClick($(this),textElement);
							    })
							)
						}
						else
						{
							//normal textarea handling otherwise
							textElement.parent().append(
								$('<img src="'+chrome.runtime.getURL('img/icon.png')+'" height="25px" width="25px" class="chatterly-translate-btn">').on('click', function(e){

							      	handleChatterlySettingsClick($(this),textElement);
							    })
							)
						}
						

						textElement.addClass("chatterly-ui-handler");

					})
      	}
      	else{
      		//remove image icons and handlers if extension is disabled
      		$(".chatterly-translate-btn").remove();
      		$(".chatterly-ui-handler").removeClass("chatterly-ui-handler");
      	}       

    });

}

// translate settings btn click handler
function handleChatterlySettingsClick(element,textElement){

  	console.log("translate requested");

  	chrome.storage.local.get(['catchyEnabled'],function(obj)
  	{

		if(obj == undefined || obj.catchyEnabled == undefined || obj.catchyEnabled == true)
		{

	  	//position window then display
	  	var selectionEl = $("#chatterly-translate-ui-popup-container").get(0);

	  	if(element.offset().left + $("#chatterly-translate-ui-popup-container").width() < $(window).width())
				selectionEl.style.left = element.offset().left + "px";
			else
				selectionEl.style.left = (element.offset().left- $("#chatterly-translate-ui-popup-container").width()) + "px";

			if(element.offset().top + $("#chatterly-translate-ui-popup-container").height()< $(window).height())
		    	selectionEl.style.top = (element.offset().top + popupYMargin)+ "px";
		    else
		    	selectionEl.style.top = ((element.offset().top + popupYMargin) - $("#chatterly-translate-ui-popup-container").height()) + "px";

	  	$("#chatterly-translate-ui-popup-container").show();

	  	//add class to detect clicked on icon
	  	$(".chatterlySelectedImg").removeClass("chatterlySelectedImg");
	  	element.addClass("chatterlySelectedImg");

	  	currentTextElement = textElement;
  	}
	});
}

function handleSelection(event) {

	chrome.storage.local.get(['catchyEnabled'],async function(obj){

		if(obj == undefined || obj.catchyEnabled == undefined || obj.catchyEnabled == true)
		{
			if(isChatterlyClicked && $("#chatterly-translate-result-ui-popup-container").is(":visible"))
			{
				isChatterlyClicked = false;
				return;
			}

			//handling case after text replacement is selected when chatterly popup is closed
			if(!$("#chatterly-translate-result-ui-popup-container").is(":visible"))
				isChatterlyClicked = false;

			var selectionEl = $("#chatterly-translate-result-ui-popup-container").get(0);

			textHighlighted = true;
			resetFlags();

			var isEmptySelection = true;

			var tempText = "", sel;
			if (window.getSelection) {
		        tempText = "" + window.getSelection();
		    } else if ( (sel = document.selection) && sel.type == "Text") {
		        tempText = sel.createRange().text;
		    }

			if(tempText && tempText.trim() != '')
			{
				isEmptySelection = false;
				//selectedElement = selectableElements[i];
				textRequested = tempText.trim();
				TranslateResultPopup.textRequested = textRequested;

			}

			if(isEmptySelection)
			{
				selectedElement = undefined;
				textRequested = "";
				TranslateResultPopup.textRequested = textRequested;

				//hide popup
				closeChatterlyPopup();
				isChatterlyClicked = false;

				return;
			}

			console.log(textRequested);

			//check if text larger than 200, update range
			if(textRequested.length > maxSringLimit)
			{
				//show warning for only 200 char limit
				showMaxLimitWarning();
				return;
			}
			

			if(event == undefined)
			{
				if(currentMousePos.x + $("#chatterly-translate-result-ui-popup-container").width() < $(window).width())
					selectionEl.style.left = currentMousePos.x + "px";
				else
					selectionEl.style.left = (currentMousePos.x - $("#chatterly-translate-result-ui-popup-container").width()) + "px";

				if(currentMousePos.y + $("#chatterly-translate-ui-popup-container").height()< $(window).height())
			    	selectionEl.style.top = (currentMousePos.y + popupYMargin)+ "px";
			    else
			    	selectionEl.style.top = ((currentMousePos.y + popupYMargin) - $("#chatterly-translate-result-ui-popup-container").height()) + "px";
			}
			else
			{
				selectionEl.style.left = event.pageX + "px";
			  selectionEl.style.top = (event.pageY + popupYMargin)+ "px";

			}

	    //update text then show popup
	    translateRequest(textRequested,1);
	    	    
			
		}
		
	});

	
}

function resetFlags(){
	hasSelectionChanged = true;
	cachedNote = false;
}

function showMaxLimitWarning(){
	swal.fire({
      title: "Character limit is "+maxSringLimit,
      text: "Max number of characters exceeded",
      icon:  "warning",
      timer: 3000,
      position: 'top-end',
	  backdrop: false,
    });
}

function closeChatterlyPopup(allWindows = false)
{
	$("#chatterly-translate-result-ui-popup-container,#chatterly-translate-ui-popup-container").hide();
	textHighlighted = false;

	//reset rating for mvp
	translateResultApp.closePopup();
}

function translateRequest(text, updatePopup = 0, element = undefined)
{
	var apiKey = "AIzaSyAuCKNYwQSFhL7oCfVYCTey29DNMHYP4-4";

	var source = (translateSettingsApp.defaultLanguageSelection == "en") ? "hi":"en";
	var target = (translateSettingsApp.defaultLanguageSelection == "en") ? "en":"hi";

  var body = {
  	q:text,
  	source:source,
  	target:target,
  	format: "text",
  }

  fetch('https://translation.googleapis.com/language/translate/v2?key='+apiKey, {
    method: 'post',
    body: JSON.stringify(body),
    headers: {
    	'Content-Type': 'application/json',
    	//'Authorization': "Bearer "+token,
    },
  }).then(function(response) {
    return response.json();
  }).then(function(data) {
    console.log('Created Gist:', data);

    TranslateResultPopup.translatedText = "";

    if(data["data"] && data["data"]["translations"].length > 0)
    	TranslateResultPopup.translatedText = data["data"]["translations"][0]["translatedText"];

    //update and show popup
    if(updatePopup == 1)
    {
    	$("#chatterly-text-translated").text(TranslateResultPopup.translatedText);
	    $("#chatterly-translate-result-ui-popup-container").show();	
    }
    //update textfield with element focused
    else if(updatePopup == 2)
    {
    	element.text(TranslateResultPopup.translatedText);
  		element.val(TranslateResultPopup.translatedText);
    }
    //update textfield
    else if(updatePopup == 3)
    {
    	updateField(TranslateResultPopup.translatedText);
    }
  });

  return TranslateResultPopup.translatedText;
}

function getTextFieldContent()
{
	var text = "";
	if(url.includes("slack"))
  {
  	text = $(".chatterlySelectedImg").parent().find(".ql-editor").first().text();
  }
  else{
  	//handle other sites
  	text = currentTextElement.val();
  }

  return text;
}

async function translateAndUpdateField()
{
	//load text first
	var text = getTextFieldContent();
  translateRequest(text,3);
  
}

function updateField(translatedText)
{
	if(url.includes("slack"))
  {
  	$(".chatterlySelectedImg").parent().find(".ql-editor").first().text(translatedText);
  }
  else{
  	currentTextElement.val(translatedText);
  }
}

//handles translation vote updates to the server
function updateVote(num)
{

	var body = {
    text:textRequested,
    translation:TranslateResultPopup.translatedText,
    vote:num,
  }

  var headers = {
    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
  }

  //send translation request
  chrome.runtime.sendMessage({"command": "server-request", body:body, serverUrl:serverUrl, headers:headers}); 
}
